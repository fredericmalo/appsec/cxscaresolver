# CxScaResolver

Dockerized Checkmarx ScaResolver

https://checkmarx.atlassian.net/wiki/spaces/CD/pages/2010809574/CxSCA+Resolver

With dependency managers. Please use the correct tag

* registry.gitlab.com/fredericmalo/appsec/cxscaresolver:maven
* registry.gitlab.com/fredericmalo/appsec/cxscaresolver:pip
* registry.gitlab.com/fredericmalo/appsec/cxscaresolver:npm